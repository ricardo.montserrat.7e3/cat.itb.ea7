import java.io.Serializable;

public class Empleado implements Serializable {
    private int id, departamento;
    private String apellido;
    private double salario;

    public Empleado(int id, int departamento, String apellido, double salario) {
        this.id = id;
        this.departamento = departamento;
        this.apellido = apellido;
        this.salario = salario;
    }

    public int getId() {return id;}

    public int getDepartamento() {return departamento;}

    public String getApellido() {return apellido;}

    public double getSalario() {return salario;}

    public void setId(int id) {this.id = id;}

    public void setDepartamento(int departamento) {this.departamento = departamento;}

    public void setApellido(String apellido) {this.apellido = apellido;}

    public void setSalario(double salario) {this.salario = salario;}

    @Override
    public String toString() {
        return "Empleado {" +
                "id: " + id +
                ", departamento: " + departamento +
                ", apellido: " + apellido + '\'' +
                ", salario: " + salario +
                '}';
    }
}
