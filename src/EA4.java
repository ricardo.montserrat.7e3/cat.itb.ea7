import java.io.*;

public class EA4
{
    public static void EscribirFichObjectEmp()
    {
        try(ObjectOutputStream dataOS = new ObjectOutputStream(new FileOutputStream(new File("Empleados.dat"))))
        {
            Empleado[] empleados = {
                    new Empleado(1, 10, "Ivan", 12),
                    new Empleado(2, 11, "Andres", 45),
                    new Empleado(3, 12, "David", 3000),
                    new Empleado(4, 12, "Raul", 4089),
                    new Empleado(5, 14, "Joel", 0)
            };

            System.out.println("GRABO LOS DATOS DE EMPLEADO.");
            for (Empleado empleado : empleados)
            {
                dataOS.writeObject(empleado);
                System.out.println("Grabo los datos del empleado." + empleado.getId() + ", "+empleado.getApellido());
            }
            System.out.println("File Employee succesfully created!");
        } catch (Exception e){ System.out.println(e.toString()); }
    }


    public static void EscribirFichObjectDep()
    {
        try(ObjectOutputStream dataOS = new ObjectOutputStream(new FileOutputStream(new File("Departamentos.dat"))))
        {
            Departamento[] departamentos = {
                    new Departamento(10, "Gerencia","Terrasa"),
                    new Departamento(11, "Comptabilitat","Badalona"),
                    new Departamento(12, "Informatica","Barcelona"),
                    new Departamento(13, "Comercial","Berga"),
                    new Departamento(14, "Auditoria","Andorra")
            };

            System.out.println("GRABO LOS DATOS DE EMPLEADO.");
            for (Departamento empleado : departamentos)
            {
                dataOS.writeObject(empleado);
                System.out.println("Grabo los datos del departamento." + empleado.getId() + ", "+empleado.getNombre());
            }
            System.out.println("File Departments succesfully created!");
        } catch (Exception e){ System.out.println(e.toString()); }
    }

    public static void LeerFichObjecteEmp() {
        Empleado empleado; // defino la variable persona
        int nunEmp=5;
        File fichero = new File("Empleados.dat");
        try {
            ObjectInputStream femp= new ObjectInputStream(new FileInputStream(fichero));
            for(int i=0;i<nunEmp;i++) {
                empleado = (Empleado) femp.readObject();
                System.out.println("Id: " + empleado.getId() + " Cognom: " + empleado.getApellido() + " Departament: " + empleado.getDepartamento() + " Salari: " + empleado.getSalario());
            }
        }catch (Exception e ){
            System.out.println(""+e);
        }
    }

    public static void LeerFichObjecteDep() {
        Departamento departamento; // defino la variable persona
        int nunDep=5;
        File fichero = new File("Departamentos.dat");
        try {
            ObjectInputStream fdep= new ObjectInputStream(new FileInputStream(fichero));
            for(int i=0;i<nunDep;i++) {
                departamento = (Departamento) fdep.readObject();
                System.out.println("Id: " + departamento.getId() + " Nom: " + departamento.getNombre() + " Localitat: " + departamento.getLocalidad());
            }
        }catch (Exception e ){
            System.out.println(""+e);
        }
    }
}

