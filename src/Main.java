import com.thoughtworks.xstream.XStream;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class XMLManager<T>
{
    @SuppressWarnings("unchecked")
    void createXML(File pathf)
    {
        List<T> list = new ArrayList<>();
        try (ObjectInputStream dataIn = new ObjectInputStream(new FileInputStream(pathf)))
        {
            System.out.println("Started Adding Objects to list ...");
            try
            {
                T obj;
                while ((obj = (T) dataIn.readObject()) != null) { list.add(obj); }
            }
            catch (ClassNotFoundException e) { System.out.println("Finished adding Objects to list!"); }
            catch (Exception e) { System.out.println(e.toString()); }
        }
        catch (Exception e) { System.out.println("Finished adding objects!"); }

        try
        {
            new XStream().toXML(list, new FileOutputStream(pathf.getName() + ".xml"));
        }
        catch (Exception e) {System.out.println("File " + pathf.getName() + ".xml Created Successfully!");}
    }

    @SuppressWarnings("unchecked")
    void readXML(File pathf)
    {
        if (pathf.exists() && pathf.isFile())
        {
            List<T> list = new ArrayList<>();
            try
            {
                 list = (List<T>) new XStream().fromXML(new FileInputStream(pathf));
            }
            catch (Exception e) { System.out.println(e.toString()); }

            System.out.println(pathf.getName() + ": \n{");

            for (T item : list)
            {
                System.out.println(item);
            }

            System.out.println('}');
        }
        else {System.out.println("The file of path: " + pathf + "/ doesn't exist yet, please create it first!");}
    }
}

public class Main
{

    private void ex1()
    {
        System.out.println("Creating Employees file...");
        EA4.EscribirFichObjectEmp();
        System.out.println("Creating Departments file...");
        EA4.EscribirFichObjectDep();

        System.out.println("Creating Employees XML version...");
        new XMLManager<Empleado>().createXML(new File("Empleados.dat"));
        System.out.println("Creating Departments XML version...");
        new XMLManager<Departamento>().createXML(new File("Departamentos.dat"));
    }

    private void ex2()
    {
        System.out.println("Trying to read Employees XML version...");
        new XMLManager<Empleado>().readXML(new File("Empleados.dat.xml"));
        System.out.println("Trying to read Departments XML version...");
        new XMLManager<Departamento>().readXML(new File("Departamentos.dat.xml"));
    }

    private void pause()
    {
        System.out.print("\n\nPress enter to continue...");
        new Scanner(System.in).nextLine();
    }

    @SuppressWarnings("SameParameterValue")
    private String inputAny(String message)
    {
        System.out.print(message);
        return new Scanner(System.in).nextLine();
    }

    private boolean getInputMenu(String userSelection)
    {
        switch (userSelection.toLowerCase())
        {
            case "1": case "create": case "c": ex1();
            break;
            case "2": case "read": case "r": ex2();
            break;
            case "3": case "exit": case "e": return true;
            default: return false;
        }
        pause();
        return false;
    }

    private void showMenu()
    {
        System.out.print("---------- Welcome To My Program ----------\n\n" +
                "Ex1[create][c] .- Create XML of Departments.dat and Employees.dat.\n" +
                "Ex2[read][r]   .- Read XML of Departments.dat and Employees.dat.\n" +
                "Mo3[exit][e]   .- Exit.\n");
    }

    private void menu()
    {
        boolean finished;
        do
        {
            showMenu();
            finished = getInputMenu(inputAny("\nSelect an option by [command] or number: "));
        }
        while (!finished);
        System.out.println("\nGoodbye, closing program :D!");
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.menu();
    }

}
