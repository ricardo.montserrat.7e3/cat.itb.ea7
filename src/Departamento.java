import java.io.Serializable;

public class Departamento implements Serializable {
    private int id;
    private String nombre, localidad;

    public Departamento(int id, String nombre, String localidad) {
        this.id = id;
        this.nombre = nombre;
        this.localidad = localidad;
    }

    public int getId() {return id;}

    public String getNombre() {return nombre;}

    public String getLocalidad() {return localidad;}

    public void setId(int id) {this.id = id;}

    public void setNombre(String nombre) {this.nombre = nombre;}

    public void setLocalidad(String localidad) {this.localidad = localidad;}

    @Override
    public String toString() {
        return "Departamento {" +
                "id: " + id +
                ", nombre: '" + nombre + '\'' +
                ", localidad: '" + localidad + '\'' +
                '}';
    }
}
